<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CCB</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="./css/preguntas.css">
    
    @livewireStyles
    <style>
        .footer {
            height: 4rem;
            #background-color: #e2e2e2;
            text-align: center;
            color: white;
            
        }
        .footer img {
            height: 30px;
            vertical-align: text-top;
        }
    </style>
</head>
<body>
    
    @livewire('preguntas-jugar')

    @livewireScripts

<footer class="footer">
    <span>
        Desarrollado por <img style="" src="./img/intelsgoBlanco.png" alt="Intel Santiago">
    </span>
    
</footer>
</body>
</html>