<div>
    <div class="message" style="display: block; text-align: center;
        background-image: url('img/preguntasGana.gif');
                background-repeat: no-repeat;
                background-size: contain;
                background-position: center;
                background-color: #e40035;
        ">
        <h1 class="title" style="display: block;">¡Increíble, ganaste!</h1>
        <p class="text" style="display: block;">Resolviste la palabra</p>
        <button wire:click="$emit('refrezcar')" class="restart button" style="display: inline-block;">¿Juega de nuevo?</button>

    </div>
    <script>
       // new Audio('./sound/gana.mp3').play()
       $(function(){
        $('#soundGana')[0].play()

        $('.restart').click(function(){
            $('#soundGana')[0].pause()
            $('#soundGana')[0].currentTime=0
        })
       })
        
        //document.getElementById("myAudio")
    </script>
</div>
