<div>
  
  <audio id="soundGana" src="./sound/gana.mp3"></audio>
  <audio id="soundPierde" src="./sound/perdedor.mp3"></audio>
    <div class="hangman">
      <ul class="word2">
          <li class="letter correct">{{ $pregunta }}</li>
      </ul>
      <div class="guess">
        <ul class="word">
          @foreach ($letras as $item)
          <li data-pos="0" class="letter">{{ $item }}</li>
          
          @endforeach
        </ul>
        
      </div>
      <form class="guessForm" wire:submit.prevent="checkLetra">
          <input wire:model="letra" type="text" class="guessLetter" maxlength="1" placeholder="Ingrese una letra . . . ⏎">
          <button type="submit" class="guessButton">Adivinar</button>
      </form>  
      
      @if (!empty($letrasIncorrectas))
      <div class="wrong">
        <ul class="wrongLetters">
          <p>Letras incorrectas: </p>
          @foreach ($letrasIncorrectas as $item)
            <li>{{ $item }}</li>
            <script>
              new Audio('./sound/bad.mp3').play()
            </script>
          @endforeach
        </ul>
      </div>
      @endif
      
  </div>
  @if ($resultado=='gana')
      @livewire('preguntas-jugar-gana', ['intentos' => $iLose+$iWin])
  @endif
  @if ($resultado=='pierde')
      @livewire('preguntas-jugar-pierde', ['respuesta' => $respuestaMostrar])
  @endif
  @foreach ($letrasCorrectas as $item)
  <script>
    new Audio('./sound/goodbell.mp3').play()
  </script>
  @endforeach 
      <!--https://s3-us-west-2.amazonaws.com/s.cdpn.io/74196/goodbell.mp3-->
      <!--https://s3-us-west-2.amazonaws.com/s.cdpn.io/74196/bad.mp3-->
      <!--https://s3-us-west-2.amazonaws.com/s.cdpn.io/74196/win.mp3-->
      <!--https://s3-us-west-2.amazonaws.com/s.cdpn.io/74196/lose.mp3-->
      <script src="./js/jquery.min.js"></script>

</div>