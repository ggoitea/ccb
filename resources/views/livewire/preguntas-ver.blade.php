<div class="card">
    <div class="card-body">
      <h5 class="card-title">
        
      </h5>

      <table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Pregunta</th>
              <th>Respuesta</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
              @foreach ($preguntas as $item)
              <tr data-id="{{ $item->id }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->pregunta }}</td>
                <td>{{ $item->respuesta }}</td>
                <td><button wire:click="delete({{ $item->id }})" class="btn btn-sm bt-circle btn-danger btnBorrar"><i class="fas fa-minus-circle"></i></button></td>
                
              </tr>
              @endforeach
            
            
          </tbody>
        </table>
    </div>
  </div>
  