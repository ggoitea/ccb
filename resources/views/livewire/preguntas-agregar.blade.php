<div class="card">
    <div class="card-body">
      <h5 class="card-title">
          
      </h5>

      <form wire:submit.prevent="store" method="POST">
        <div class="form-group">
            <label>Pregunta</label>
            <input wire:model="pregunta" type="text" class="form-control" placeholder="Ingresar la pregunta ..." required>
        </div>
        <div class="form-group">
            <label>Respuesta</label>
            <input wire:model="respuesta" type="text" class="form-control" placeholder="Ingresar la respuesta ..." required>
        </div>
        <button type="submit" class="card-link">agregar</button>
      </form>

      
    </div>
  </div>