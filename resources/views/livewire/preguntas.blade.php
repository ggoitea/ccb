<div>
    

    @if ($componente=='ver')
    <button wire:click="agregar" class="btn btn-sm btn-circle btn-outline-primary"><i class="fa fa-plus"></i></button>
        @livewire('preguntas-ver')
    @endif
    @if ($componente=='agregar')
        <button wire:click="ver" class="btn btn-sm btn-circle btn-outline-primary"><i class="fas fa-arrow-circle-left"></i></button>

        @livewire('preguntas-agregar')
    @endif
</div>
