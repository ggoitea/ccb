<div>
    <div class="message" style="display: block;text-align: center;">
        <h1 class="title" style="display: block;margin-bottom: 0px;">Perdiste.. La palabra era <span class="highlight">{{ $respuesta }}</span></h1>
        <img src="img/juegos/preguntas/emojiPierde.gif" alt="" style="width: 20%;">
        <p class="text" style="display: block;margin-top: 0px;margin-bottom: 10px;">¡No te preocupes, obtendrás el siguiente!</p>
        <button wire:click="$emit('refrezcar')" class="restart button" style="display: inline-block;margin-top: 0px;">¿Juega de nuevo?</button>

    </div>
    <script>
        //new Audio('./sound/perdedor.mp3').play()
        $(function(){
            $('#soundPierde')[0].play()
            $('.reset').click(function(){
                $('#soundPierde')[0].pause()
                $('#soundPierde')[0].currentTime=0
            })
        })
    </script>
</div>
