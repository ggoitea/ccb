<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CCB</title>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="./css/memory.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .footer {
            height: 4rem;
            #background-color: #e2e2e2;
            text-align: center;
            
        }
        .footer img {
            height: 30px;
            vertical-align: text-top;
        }
    </style>

</head>
<body>
    <div class="wrap">
        <div class="game"></div>

        <div class="modal-overlay">
            <audio id="soundGana" src="./sound/gana.mp3"></audio>
            <div class="modal" style="background-image: url('img/preguntasGana.gif');
                background-repeat: no-repeat;
                background-size: contain;
                background-position: center;
                background-color: #e40035;">                
                <div class="message" style="display: block; text-align: center;" >
                    <h1 class="title" style="display: block;">¡Increíble, ganaste!</h1>
                    <button  class="restart" style="display: inline-block;">¿Juega de nuevo?</button>
                </div>
            </div>
        </div>

    </div><!-- End Wrap -->
<footer class="footer">
    <span>
        Desarrollado por <img style="" src="./img/intelsgoBlanco.png" alt="Intel Santiago">
    </span>
</footer>
<script src="./js/jquery.min.js"></script>
<script src="./js/memory.js"></script>

</body>
</html>