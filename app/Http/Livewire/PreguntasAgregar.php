<?php

namespace App\Http\Livewire;

use App\Pregunta;
use Livewire\Component;

class PreguntasAgregar extends Component
{
    public $pregunta;
    public $respuesta;

    public function render()
    {
        return view('livewire.preguntas-agregar');
    }

    public function store(){
        $pregunta=new Pregunta();
        $pregunta->pregunta=$this->pregunta;
        $pregunta->respuesta=$this->respuesta;
        $pregunta->save();
        $this->emit('ver');
        $this->emit('reload');
    }
}
