<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MemoriaJugar extends Component
{
    public function render()
    {
        return view('livewire.memoria-jugar');
    }
}
