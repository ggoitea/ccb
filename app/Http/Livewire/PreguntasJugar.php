<?php

namespace App\Http\Livewire;

use App\Pregunta;
use Livewire\Component;
use Illuminate\Support\Str;

class PreguntasJugar extends Component
{
    public $pregunta;
    public $respuesta;
    public $letras;
    public $letra='';
    public $posiciones;
    public $iWin=0;
    public $iLose=0;
    public $resultado='';
    public $letrasIncorrectas=array();
    public $letrasCorrectas=array();
    public $respuestaMostrar;
    public $yaJugadas=array();
    public $preguntas;

    protected $listeners = ['refrezcar','checkLetra'];

    public function mount(){
        $pregunta=$this->azar();
        $this->pregunta=$pregunta['pregunta'];
        $this->respuesta=$pregunta['respuesta'];
        $this->respuestaMostrar=$pregunta['respuesta'];
        $this->letras();
    }

    public function render()
    {
        return view('livewire.preguntas-jugar');
    }

    public function azar(){
        if(!$this->preguntas){
            $this->preguntas=Pregunta::all();

        }
        $pregunta=$this->preguntas->random();

        $this->preguntas=$this->preguntas->where('id','!=',$pregunta->id);
        return $pregunta->toArray();
    }
    public function refrezcar(){
        if(empty($this->preguntas->toArray())){
            $this->preguntas=Pregunta::all();

        }
        $pregunta=$this->azar();
        $this->pregunta=$pregunta['pregunta'];
        $this->respuesta=$pregunta['respuesta'];
        $this->respuestaMostrar=$pregunta['respuesta'];
        $this->letras();
        $this->iWin=0;
        $this->iLose=0;
        $this->letra='';
        $this->resultado='';
        $this->letrasIncorrectas=array();
        $this->letrasCorrectas=array();
    }

    public function letras(){
        $letras=array();
        for ($i=0; $i < strlen($this->respuesta); $i++) { 
            $letras[]='*';
        }

        $this->letras=$letras;
    }

    public function checkLetra(){
        $j=0;
        $k=0;
        for ($i=0; $i < strlen($this->respuesta); $i++) { 
            if(strtoupper($this->respuesta[$i])==strtoupper($this->letra)){
                $this->letras[$i]=$this->letra;
                $this->iWin=$this->iWin+1;
                $this->respuesta[$i]='-';
                $this->letrasCorrectas[$i]='*';
            }else{
                
                $j=$j+1;
            }
        }
        if(strlen($this->respuesta)==$j){
            $this->iLose=$this->iLose+1;
            $this->letrasIncorrectas[]=strtoupper($this->letra);
        } 
        $this->letra='';
        if($this->iWin>=strlen($this->respuesta)){
            $this->resultado='gana';
        }
        if($this->iLose==5){
            $this->resultado="pierde";
        }
    }

    public function eliminarPregunta($id_eliminar){
        $preguntas= $this->preguntas->toArray();
        unset($preguntas[$id_eliminar]);
        $this->preguntas=collect($preguntas);
    }
}