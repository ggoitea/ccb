<?php

namespace App\Http\Livewire;

use App\Pregunta;
use Livewire\Component;

class PreguntasVer extends Component
{
    public $preguntas;
    protected $listeners=['reload'];

    public function mount(){
        $this->preguntas=Pregunta::all();
    }
    
    public function render()
    {
        
        return view('livewire.preguntas-ver');
    }
    
    public function reload(){
        $this->preguntas=Pregunta::all();
    }

    public function delete($id){
        Pregunta::find($id)->delete();
        $this->reload();
    }

}
