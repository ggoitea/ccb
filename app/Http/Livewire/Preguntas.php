<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Preguntas extends Component
{
    public $componente='ver';
    protected $listeners=['ver'];

    public function render()
    {
        return view('livewire.preguntas');
    }

    public function agregar(){
        $this->componente='agregar';
    }
    public function borrar(){
        $this->componente='borrar';
    }
    public function ver(){
        $this->componente='ver';
    }
    
}
