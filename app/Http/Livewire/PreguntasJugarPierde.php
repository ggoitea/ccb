<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PreguntasJugarPierde extends Component
{
    public $respuesta;
    public function render()
    {
        return view('livewire.preguntas-jugar-pierde');
    }
}
