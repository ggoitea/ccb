<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PreguntasBorrar extends Component
{
    public function render()
    {
        return view('livewire.preguntas-borrar');
    }
}
