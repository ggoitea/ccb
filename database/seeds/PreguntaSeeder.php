<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreguntaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('preguntas')->insert([
            'pregunta' => '¿Antiguo pueblo originario que da el nombre a esta sala?',
            'respuesta' => 'matara'
        ]);
        DB::table('preguntas')->insert([
            'pregunta' => '',
            'respuesta' => ''
        ]);
    }
}
