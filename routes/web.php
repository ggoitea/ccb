<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('preguntas');
});

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');


//aqui comienza
Route::middleware('auth')->prefix('panel')->group(function(){
    Route::get('/',function(){
        return redirect()->route('preguntas.index');
    });
    Route::resource('/preguntas','PreguntaController');
});
Route::get('/preguntas', 'PreguntaController@juego')->name('preguntas');
Route::get('/memoria', 'MemoriaController@juego')->name('memoria');
